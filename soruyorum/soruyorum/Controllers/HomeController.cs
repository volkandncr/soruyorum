﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace soruyorum.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            

            return View();
        }

        public ActionResult Contact()
        {
            

            return View();
        }

        public ActionResult Cozum()
        {
            

            return View();
        }

        public ActionResult SoruSor()
        {
            return View();
        }
        public ActionResult SoruCoz()
        {
            return View();
        }
        public ActionResult Iletisim()
        {
            return View();
        }
        public ActionResult Iletisim2()
        {
            return View();
        }
        public ActionResult Kategori()
        {
            return View();
        }
        public ActionResult DilDegistir(string dil)
        {
            if (dil != null)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(dil);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(dil);

            }

            HttpCookie cookie = new HttpCookie("Dil");
            cookie.Value = dil;
            Response.Cookies.Add(cookie);

            return View("Index");
        }

    }
}