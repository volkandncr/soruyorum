﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace soruyorum.Dil {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class dil {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal dil() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("soruyorum.Dil.dil", typeof(dil).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Family.
        /// </summary>
        public static string aile {
            get {
                return ResourceManager.GetString("aile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Home.
        /// </summary>
        public static string anasayfa {
            get {
                return ResourceManager.GetString("anasayfa", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Crossword.
        /// </summary>
        public static string bulmaca {
            get {
                return ResourceManager.GetString("bulmaca", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Solve.
        /// </summary>
        public static string çöz {
            get {
                return ResourceManager.GetString("çöz", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Lang.
        /// </summary>
        public static string Dil1 {
            get {
                return ResourceManager.GetString("Dil1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Education.
        /// </summary>
        public static string egitim {
            get {
                return ResourceManager.GetString("egitim", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Login.
        /// </summary>
        public static string giris {
            get {
                return ResourceManager.GetString("giris", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Send.
        /// </summary>
        public static string gönder {
            get {
                return ResourceManager.GetString("gönder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Contact.
        /// </summary>
        public static string iletişim {
            get {
                return ResourceManager.GetString("iletişim", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Category.
        /// </summary>
        public static string kategori {
            get {
                return ResourceManager.GetString("kategori", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Register.
        /// </summary>
        public static string kaydol {
            get {
                return ResourceManager.GetString("kaydol", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to username.
        /// </summary>
        public static string kullanıcıadı {
            get {
                return ResourceManager.GetString("kullanıcıadı", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Culture.
        /// </summary>
        public static string kültür {
            get {
                return ResourceManager.GetString("kültür", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mathematics.
        /// </summary>
        public static string matematik {
            get {
                return ResourceManager.GetString("matematik", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Art.
        /// </summary>
        public static string sanat {
            get {
                return ResourceManager.GetString("sanat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Forget your password?.
        /// </summary>
        public static string şifrenizimiunuttunuz {
            get {
                return ResourceManager.GetString("şifrenizimiunuttunuz", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Complaint.
        /// </summary>
        public static string sikayet {
            get {
                return ResourceManager.GetString("sikayet", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Political.
        /// </summary>
        public static string siyasi {
            get {
                return ResourceManager.GetString("siyasi", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Question Solve.
        /// </summary>
        public static string soruçöz {
            get {
                return ResourceManager.GetString("soruçöz", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Solve to the question.
        /// </summary>
        public static string sorununçözumleri {
            get {
                return ResourceManager.GetString("sorununçözumleri", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ask the question.
        /// </summary>
        public static string sorunusor {
            get {
                return ResourceManager.GetString("sorunusor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ask question.
        /// </summary>
        public static string sorusor {
            get {
                return ResourceManager.GetString("sorusor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Social.
        /// </summary>
        public static string sosyal {
            get {
                return ResourceManager.GetString("sosyal", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sport.
        /// </summary>
        public static string spor {
            get {
                return ResourceManager.GetString("spor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to All Rights Reserved.
        /// </summary>
        public static string tümhaklarisaklidir {
            get {
                return ResourceManager.GetString("tümhaklarisaklidir", resourceCulture);
            }
        }
    }
}
